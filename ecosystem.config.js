require('dotenv').config()
module.exports = {
	apps: [
		{
			name: `RD ${process.env.PRODUCT} ${process.env.ENVIRONMENT}  API - ${process.env.PORT}`,
			script: './src/app.js',
			args: 'one two',
			instances: `${process.env.PM2INSTANCES}`,
			autorestart: true,
			// watch: ["server", "client"],
			watch: true,
			max_memory_restart: '1G',
			env: {
				NODE_ENV: 'production',
			},
			env_production: {
				NODE_ENV: 'production',
			},
		},
	],
}
